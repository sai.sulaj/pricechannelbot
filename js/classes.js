/**
 * Author: Saimir Sulaj.
 * Date: March 15, 2018.
 * Purpose: File to hold javascript classes for CogniSynth Capital Arbitrage Bot.
 */

// -------------- DEPENDENCIES -------------- //

const config                = require('config');

// -------------- CONSTANTS & GLOBAL VARS -------------- //

const DEBUG                 = config.get('debug');
const HISTORY_BUFFER_LENGTH = 30;
const PRICE_INDEX           = 0;
const VOL_INDEX             = 1;

// -------------- CLASSES -------------- //

function ExchangeData (_Ccxt, _TradingFee) {
  if (DEBUG) { console.log(':ExchangeData'); }
  
  this.ccxt                 = _Ccxt;
  this.orderbookHistory     = {};
  this.tradingFee           = _TradingFee;
}

ExchangeData.prototype = {
  /**
   * @dev -> Fetches a complete readable string of the exchange's name.
   * @returns -> Readable sentence with exchange's name.
   */
  getReadableInfo: function () {
    return `Exchange name: ${this.ccxt.name}`;
  },
  /**
   * @dev -> Fetches latest orderbook data cached in :ExchangeData.
   * @param (str) _Symbol -> Symbol to fetch orderbook data from.
   * @param (int) _NumEntries -> Number of orderbook data entries requested. If more than available data, will return all data.
   * @returns NULLABLE (array) -> Array with orderbook data of requested entries or less.
   */
  retrieveOrderbookData: function (_Symbol, _NumEntries) {
    if (DEBUG) { console.log(`:ExchangeData#retrieveOrderbookData -> _Symbol:  ${_Symbol}, _NumEntries: ${_NumEntries}`); }
    _Symbol = _Symbol.toLowerCase();
    
    if (!this.orderbookHistory.hasOwnProperty(_Symbol) || this.orderbookHistory[_Symbol].length == 0) { return null; }
    let numEntries = _NumEntries > this.orderbookHistory[_Symbol].length ? this.orderbookHistory[_Symbol].length : _NumEntries;
    return this.orderbookHistory[_Symbol].slice(0, numEntries);
  },
  
  updateOrderbookData: function (_Symbol, _Data) {
    if (DEBUG) { console.log(`:exchangeData#updateOrderbookData -> _Symbol: ${_Symbol}, _Data: ${_Data}`); }
    _Symbol = _Symbol.toLowerCase();
    
    if (this.orderbookHistory.hasOwnProperty(_Symbol)) {
      this.orderbookHistory[_Symbol].unshift(_Data);
      if (this.orderbookHistory.length >= HISTORY_BUFFER_LENGTH) {
        this.orderbookHistory[_Symbol].pop();
      }
    } else {
      this.orderbookHistory[_Symbol] = [];
      this.orderbookHistory[_Symbol].unshift(_Data);
    }
  },
  
  getTradingFee: function () {
    if (DEBUG) { console.log(':exchangeData#getTradingFee'); }
    return this.tradingFee;
  }
}

function Analysis() {}

Analysis.prototype = {
  getROI: function(_AskPrice, _BidPrice) {
    if (DEBUG) { console.log(`:Analysis#getROI -> _AskPrice: ${_AskPrice}, _BidPrice: ${_BidPrice}`); }
    return _BidPrice / _AskPrice;
  },
  
  getSpread: function(_OrderbookData) {
    if (DEBUG) { console.log(`:Analysis#getSpread`); }
    let lowestAsk   = _OrderbookData[0]['asks'][0][PRICE_INDEX];
    let highestBid  = _OrderbookData[0]['bids'][0][PRICE_INDEX];
    return (lowestAsk / highestBid) - 1;
  }
}

module.exports = {
  ExchangeData: ExchangeData,
  Analysis: Analysis
}