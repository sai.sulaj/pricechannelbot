/*
 * Author: Saimir Sulaj.
 * Date: April 7, 2018.
 * Purpose: Bot that scans crypto markets and alerts users on detection of price channels.
 *          Built for Cognisynth Capital.
 */

// [x] TODO: Alert user if exchange does not support symbol.
// [x] TODO: Apply displatchNotification function.

// -------------- DEPENDENCIES -------------- //

const CCXT                    = require('ccxt');
const config                  = require('config');
const TI                      = require('technicalindicators');
const stats                   = require('stats-lite');
const firebaseAdmin           = require('firebase-admin');

const classes                 = require('./js/classes.js');

// -------------- GLOBAL VARS & CONSTANTS -------------- //

const DEBUG                   = config.get('debug');
const TESTING                 = config.get('testing');
const UPDATE_PERIOD           = config.get('update_period');
const PRICE_INDEX             = 0;
const VOL_INDEX               = 1;
const DELTA_PRICE_THRESHOLD   = 0.1;
const ST_DEV_MUL              = 3;

var   activeExchanges         = [];
var   activeSymbols           = getActiveSymbols();
var   symbolIndex             = 0;
var   exchangeIndex           = 0;

// Firebase admin init.
let firebaseServiceAccount    = require('./cognisynth-dashboard-firebase-adminsdk-apdx1-5c8b3ae79a.json');
firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(firebaseServiceAccount),
  databaseURL: "https://cognisynth-dashboard.firebaseio.com"
});

const firebaseDatabase        = firebaseAdmin.database();

// -------------- MAIN -------------- //

(async () => {
  // Loading exchanges and market data.
  activeExchanges = await getActiveExchanges();
    
  printIntroduction(1);
  printActiveExchanges(1);
  printActiveSymbols(1);
  
  if (!checkConfigData()) { return; }
  
  // Main loop.
  var continueRunning = true;
  while (continueRunning) {
    try {
      if (DEBUG) { console.log(`EXCHANGE INDEX: ${exchangeIndex}, EXCHANGE NAME: ${activeExchanges[exchangeIndex].ccxt.name}, SYMBOL INDEX: ${symbolIndex}, SYMBOL NAME: ${activeSymbols[symbolIndex]}`); }
      
      let data = await getOrderbookData(activeExchanges[exchangeIndex].ccxt.name, activeSymbols[symbolIndex]);
      activeExchanges[exchangeIndex].updateOrderbookData(activeSymbols[symbolIndex], data);
      
      let switchExchange = incrementExchangeAndSymbolIndices();
      if (switchExchange) {
        analyzeData()
        await new Promise (resolve => setTimeout(resolve, UPDATE_PERIOD));
      }
      printBlankLines(2);
    } catch (err) {
      console.log(err);
      continueRunning = false;
    }
  }
}) ();

// -------------- FUNCTIONS -------------- //

/**
 * @dev -> Analyzes data and reports anomalies.
 */
function analyzeData() {
  if (DEBUG) { console.log(`#analyzeData`); }
  
  for (var tmpExI = 0; tmpExI < activeExchanges.length; tmpExI++) {
    let exchange = activeExchanges[tmpExI];
    for (var tmpSybI = 0; tmpSybI < activeSymbols.length; tmpSybI++) {
      let symbol = activeSymbols[tmpSybI];
      let orderbookData = exchange.retrieveOrderbookData(symbol, 1);
      if (orderbookData == null || orderbookData[0] == null) { continue; }
      
      if (duplicateOrdersDetector(orderbookData[0]['bids'], DEBUG) || duplicateOrdersDetector(orderbookData[0]['asks'], DEBUG)) {
        console.log('DUPLICATE DETECTED');
        updateFirebaseAnomaly(exchange.ccxt.name.toLowerCase(), symbol, true);
      } else if (detectOutliersByVol(orderbookData[0]['bids'], ST_DEV_MUL, DEBUG) || detectOutliersByVol(orderbookData[0]['asks'], ST_DEV_MUL, DEBUG)) {
        console.log('VOL OUTLIER DETECTED');
        updateFirebaseAnomaly(exchange.ccxt.name.toLowerCase(), symbol, true);
      } else {
        console.log('NO ANOMALY DETECTED');
        updateFirebaseAnomaly(exchange.ccxt.name.toLowerCase(), symbol, false);
      }
    }
  }
}

/**
 * @dev -> Queries data for duplicate volumes in orders with similar prices.
 * @param (array) _Data -> Array of wither bid or ask orders.
 * @param (bool) _Verbose -> Prints anomaly to console if true.
 * @returns (bool) -> Whether or not anomaly was found.
 */
function duplicateOrdersDetector(_Data, _Verbose) {
  let volData = [];
  let queriedindices = [];
  let anomalies = [];
  
  for (var i = 0; i < _Data.length; i++) {
    volData.push(_Data[i][VOL_INDEX]);
  }
  
  for (var a = 0; a < volData.length; a++) {
    if (queriedindices.includes(a)) {
      continue;
    }
    
    let queryVal = volData[a];
    let anomaliesTmp = [a];
    let queriedIndicesTmp = [];
    
    for (var b = 0; b < volData.length; b++) {
      if (queriedindices.includes(b) || a == b || queriedIndicesTmp.includes(b)) {
        continue;
      }
      
      if (volData[b] == queryVal) {
        anomaliesTmp.push(b);
        queriedIndicesTmp.push(b);
      }
    }
    
    if (anomaliesTmp.length > 2) {
      anomalies.push(anomaliesTmp);
      queriedindices = queriedindices.concat(queriedIndicesTmp);
      // console.log(`anomaly found! anomaly: ${anomaliesTmp}, delta indices: ${queriedIndicesTmp}, new indices: ${queriedindices}`);
    }
  }
  
  for (var anomalyI = 0; anomalyI < anomalies.length; anomalyI++) {
    let anomaly = anomalies[anomalyI];
    let prices = anomaly.map((index) => { return _Data[index][PRICE_INDEX]; });
    let deltas = [];
    for (var i = 0; i < prices.length - 1; i++) { deltas.push(Math.abs(prices[i] - prices[i + 1])); }
    if (stats.stdev(deltas) < stats.mean(deltas) * DELTA_PRICE_THRESHOLD) {
      if (_Verbose) { console.log(`Duplicate anomaly found. Indices: ${anomaly}`); }
      return true;
    }
  }
  return false;
}

/**
 * @dev -> Queries data for volume anomaly.
 * @param (array) _Data -> Array of either bid or ask orders.
 * @param (bool) _Verbose -> Prints anomaly to console if true.
 * @param (int) _StDevMul -> Multiple of StDev to add to mean for threshold.
 * @returns (bool) -> Whether or not anomaly was found.
 */
function detectOutliersByVol(_Data, _StDevMul, _Verbose) {
  let outliers = [];
  let volData = [];
  
  // Remove price column.
  for (var i = 0; i < _Data.length; i++) {
    volData.push(_Data[i][VOL_INDEX]);
  }
  
  if (_Verbose) {
    console.log(`MEAN: ${stats.mean(volData)}`);
    console.log(`STDEV: ${stats.stdev(volData)}`);
    console.log(`STDEVMULTIPLE: ${_StDevMul}`);
  }
  
  let threshold = (stats.stdev(volData) * _StDevMul) + stats.mean(volData);
  for (var i = 0; i < volData.length; i++) {
    if (volData[i] > threshold) {
      outliers.push(volData[i]);
    }
  }
  if (outliers.length > 0) { 
    if (_Verbose) { console.log(`VOL OUTLIER DETECTED: ${outliers}`); }
    return true;
  }
  return false;
}

/**
 * @dev -> Increments exchange and symbol indices so that program iterates through each exchange per
 *         symbol.
 * @returns (bool) -> True if symbols have been switched.
 */
function incrementExchangeAndSymbolIndices() {
  if (exchangeIndex == activeExchanges.length - 1) {
    exchangeIndex = 0;
    if (symbolIndex == activeSymbols.length - 1) {
      symbolIndex = 0;
    } else {
      symbolIndex++;
    }
    return true;
  } else {
    exchangeIndex++;
    return false;
  }
}

/**
 * @dev -> Fetches specified symbol orderbook data from specified exchange.
 * @param (str) _ExchangeName -> Name of exchange to query.
 * @param (str) _Symbol -> Symbol to query for.
 * @returns NULLABLE (json) -> Orderbook data.
 */
async function getOrderbookData(_ExchangeName, _Symbol) {
  if (DEBUG) { console.log(`#getOrderbookData -> _ExchangeName: ${_ExchangeName}, _Symbol: ${_Symbol}`); }
  
  let exchange = activeExchanges.filter((item) => { return item.ccxt.name.toLowerCase() == _ExchangeName.toLowerCase(); })[0];
  
  // Assert exchanges supports symbol.
  if (!exchange.ccxt.symbols.includes(_Symbol)) {
    if (DEBUG) { console.log(`WARNING: ${_ExchangeName} DOES NOT SUPPORT ${_Symbol}. SKIPPING LOOP...`); }
    return null;
  }
  
  let data = await exchange.ccxt.fetchOrderBook(_Symbol).catch((err) => {
    console.log(`#getOrderbookData -> ERROR: COULD NOT FETCH ORDERBOOK DATA:\n${err}`);
    return null;
  });
  
  return data;
}

/**
 * @dev -> Checks whether program is configured sufficiently to meaningfully execute.
 *         If not, prints error to console.
 * @returns (bool) -> True if configured correctly, false if otherwise.
 */
function checkConfigData() {
  if (DEBUG) { console.log('#checkConfigData'); }
  
  // Ensure enough symbols and exchanges are enabled.
  if (activeExchanges.length == 0) {
    console.log('CONFIG ERROR: INSUFFICIENT NUMBER OF EXCHANGES ENABLED. TERMINATING PROGRAM...');
    return false;
  } else if (activeSymbols.length == 0) {
    console.log('CONFIG ERROR: INSUFFICIENT NUMBER OF SYMBOLS ENABLED> TERMINATING PROGRAM...');
    return false;
  }
  
  return true;
}

/**
 * @dev -> Retrieves active exchange data from config file, and loads markets.
 * @returns (array<ccxt exchange object>) -> Array of ccxt exchange objects woth markets loaded.
 */
async function getActiveExchanges() {
  if (DEBUG) { console.log('#getActiveExchanges'); }
  let tmpArray = [];
  
  let useBinance = config.get('active_exchanges.binance');
  if (useBinance) {
    let tradingFee = config.get('exchange_data.binance.trading_fee');
    let index = tmpArray.push(new classes.ExchangeData(new CCXT.binance(), tradingFee)) - 1;
    try {
      await tmpArray[index].ccxt.loadMarkets();
    } catch (err) {
      console.log(`ERROR: COULD NOT LOAD BINANCE MARKET DATA: ${err}`);
      console.log('REMOVING FROM ACTIVE EXCHANGES...');
      tmpArray.splice(index, 1);
    }
  }
  let useKraken = config.get('active_exchanges.kraken');
  if (useKraken) {
    let tradingFee = config.get('exchange_data.kraken.trading_fee');
    let index = tmpArray.push(new classes.ExchangeData(new CCXT.kraken(), tradingFee)) - 1;
    try {
      await tmpArray[index].ccxt.loadMarkets();
    } catch (err) {
      console.log(`ERROR: COULD NOT LOAD KRAKEN MARKET DATA: ${err}`);
      console.log('REMOVING FROM ACTIVE EXCHANGES...');
      tmpArray.splice(index, 1);
    }
  }
  let useBitfinex = config.get('active_exchanges.bitfinex');
  if (useBitfinex) {
    let tradingFee = config.get('exchange_data.bitfinex.trading_fee');
    let index = tmpArray.push(new classes.ExchangeData(new CCXT.bitfinex(), tradingFee)) - 1;
    try {
      await tmpArray[index].ccxt.loadMarkets();
    } catch (err) {
      console.log(`ERROR: COULD NOT LOAD BITFINEX MARKET DATA: ${err}`);
      console.log('REMOVING FROM ACTIVE EXCHANGES...');
      tmpArray.splice(index, 1);
    }
  }
  let useBittrex = config.get('active_exchanges.bittrex');
  if (useBittrex) {
    let tradingFee = config.get('exchange_data.bittrex.trading_fee');
    let index = tmpArray.push(new classes.ExchangeData(new CCXT.bittrex(), tradingFee)) - 1;
    try {
      await tmpArray[index].ccxt.loadMarkets();
    } catch (err) {
      console.log(`ERROR: COULD NOT LOAD BITTREX MARKET DATA: ${err}`);
      console.log('REMOVING FROM ACTIVE EXCHANGES...');
      tmpArray.splice(index, 1);
    }
  }
  let useCex = config.get('active_exchanges.cex');
  if (useCex) {
    let tradingFee = config.get('exchange_data.cex.trading_fee');
    let index = tmpArray.push(new classes.ExchangeData(new CCXT.cex(), tradingFee)) - 1;
    try {
      await tmpArray[index].ccxt.loadMarkets();
    } catch (err) {
      console.log(`ERROR: COULD NOT LOAD CEX MARKET DATA: ${err}`);
      console.log('REMOVING FROM ACTIVE EXCHANGES...');
      tmpArray.splice(index, 1);
    }
  }
  let usePoloniex = config.get('active_exchanges.poloniex');
  if (useCex) {
    let tradingFee = config.get('exchange_data.poloniex.trading_fee');
    let index = tmpArray.push(new classes.ExchangeData(new CCXT.poloniex(), tradingFee)) - 1;
    try {
      await tmpArray[index].ccxt.loadMarkets();
    } catch (err) {
      console.log(`ERROR: COULD NOT LOAD POLONIEX MARKET DATA: ${err}`);
      console.log('REMOVING FROM ACTIVE EXCHANGES...');
      tmpArray.splice(index, 1);
    }
  }
  let useCobinhood = config.get('active_exchanges.cobinhood');
  if (useCobinhood) {
    let tradingFee = config.get('exchange_data.cobinhood.trading_fee');
    let index = tmpArray.push(new classes.ExchangeData(new CCXT.cobinhood(), tradingFee)) - 1;
    try {
      await tmpArray[index].ccxt.loadMarkets();
    } catch (err) {
      console.log(`ERROR: COULD NOT LOAD POLONIEX MARKET DATA: ${err}`);
      console.log('REMOVING FROM ACTIVE EXCHANGES...');
      tmpArray.splice(index, 1);
    }
  }
  
  return tmpArray;
}

/**
 * @dev -> Retrieves active symbol data from config file.
 * @returns (array<str>) -> Array of enabled symbol strings in format: '{BASE}/{QUOTE}'.
 */
function getActiveSymbols() {
  if (DEBUG) { console.log('#getActiveSymbols'); }
  let tmpArray = [];
  
  if (config.get('active_symbols.ETH/XRP')) { tmpArray.push('ETH/XRP'); }
  if (config.get('active_symbols.ETH/VEN')) { tmpArray.push('ETH/VEN'); }
  if (config.get('active_symbols.ETH/BTC')) { tmpArray.push('ETH/BTC'); }
  if (config.get('active_symbols.XRP/ETH')) { tmpArray.push('XRP/ETH'); }
  if (config.get('active_symbols.VEN/ETH')) { tmpArray.push('VEN/ETH'); }
  if (config.get('active_symbols.BTC/ETH')) { tmpArray.push('BTC/ETH'); }
  if (config.get('active_symbols.XLM/ETH')) { tmpArray.push('XLM/ETH'); }
  if (config.get('active_symbols.ETH/XLM')) { tmpArray.push('ETH/XLM'); }
  if (config.get('active_symbols.ETH/XMR')) { tmpArray.push('ETH/XMR'); }
  if (config.get('active_symbols.XMR/ETH')) { tmpArray.push('XMR/ETH'); }
  if (config.get('active_symbols.LTC/ETH')) { tmpArray.push('LTC/ETH'); }
  if (config.get('active_symbols.DASH/ETH')) { tmpArray.push('DASH/ETH'); }
  if (config.get('active_symbols.ETH/LTC')) { tmpArray.push('ETH/LTC'); }
  if (config.get('active_symbols.ETH/DASH')) { tmpArray.push('ETH/DASH'); }
  if (config.get('active_symbols.ETH/NEO')) { tmpArray.push('ETH/NEO'); }
  if (config.get('active_symbols.NEO/ETH')) { tmpArray.push('NEO/ETH'); }
  
  return tmpArray;
}

/**
 * @dev -> Prints introduction to program with version number.
 * @param (int) _NumLines -> Number of blank linkes to print under introduction.
 */
function printIntroduction(_NumLines) {
  console.log(' ------------------------------------------ ');
  console.log('/   COGNISYNTH CAPITAL PRICE CHANNEL BOT   \\');
  console.log('\\             VERSION: 3.0.0               /');
  console.log(' ------------------------------------------ ')
  printBlankLines(_NumLines);
}

/**
 * @dev -> Prints active symbols that are being queried on the markets.
 * @param (int) _NumLines -> Number of blank linkes to print under text.
 */
function printActiveSymbols(_NumLines) {
  console.log('ACTIVE SYMBOLS:');
  for (var i = 0; i < activeSymbols.length; i++) {
    console.log(activeSymbols[i]);
  }
  printBlankLines(_NumLines);
}

/**
 * @dev -> Prints active exchanges that are being queried.
 * @param (int) _NumLines -> Number of blank linkes to print under text.
 */
function printActiveExchanges(_NumLines) {
  console.log('ACTIVE EXCHANGES:');
  for (var i = 0; i < activeExchanges.length; i++) {
    console.log(activeExchanges[i].name);
  }
  printBlankLines(_NumLines);
}

function printBlankLines(_NumLines) {
  if (_NumLines > 0) {
    console.log('\n'.repeat(_NumLines));
  }
}

function updateFirebaseAnomaly(_ExchangeName, _Symbol, _Active) {
  if (DEBUG) { console.log('#updateFirebaseAnomaly'); }
  firebaseDatabase.ref(`priceChannelBot/${_ExchangeName.replace('.', '_')}/${_Symbol.replace('/', '_')}/timeseries`).push({
    active: _Active,
    timestamp: firebaseAdmin.database.ServerValue.TIMESTAMP
  });
}